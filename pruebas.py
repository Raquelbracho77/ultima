import json
import urllib.request

#from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

url_path="https://www.mobile.de/"


def get_data(url):
	# Se define una request o solicitud, con encabezado
	# que simula ser un browser-> firefox
	req = urllib.request.Request(
		url, 
		data=None, 
		headers={
			'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
		}
	)
	# se abre un contexto que procesa la respuesta a descargar una url
	with urllib.request.urlopen(req) as response:
		# se lee la respuesta y se asigna como string html		
		html_response = response.read()  
		# se extrae la codificacion de la respuesta
		encoding = response.headers.get_content_charset('utf-8')
		# se carga la respuesta, según codificacion, a un diccionario:: string->diccionario
		decoded_html = json.loads(html_response.decode(encoding))
		# se imprime resultado
		[print(item,v) for item,v in decoded_html.items()]

carro="https://fastlane.rubiconproject.com/a/api/fastlane.json?account_id=10812&site_id=24988&zone_id=603224&size_id=15&p_pos=unknown&gdpr=1&gdpr_consent=BOgbE3xOgbE3xB9ABBDECL-AAAAmB7_______9______5uz_Ov_v_f__33e8__9v_l_7_-___u_-33d4-_1vf99yfm1-7ftr3tp_87ues2_Xur__59__3z3_9phPrsks9Kg&rf=https%3A%2F%2Fsuchen.mobile.de%2Ffahrzeuge%2Fdetails.html%3Fid%3D262811081%26damageUnrepaired%3DNO_DAMAGE_UNREPAIRED%26isSearchRequest%3Dtrue%26minFirstRegistrationDate%3D2019-01-01%26pageNumber%3D1%26scopeId%3DC%26sfmr%3Dfalse%26steered%3D1%26searchId%3De7656605-41d1-efaa-facf-f2fc20440f1b&tg_i.Marke=MITSUBISHI&tg_i.Modell=Space%20Star&tg_i.Baujahr=2019&tg_i.Channel=Car&tk_flint=pbjs_lite_v2.9.0&x_source.tid=d1057ab3-f66c-4dc0-a386-0acc84f1777d&p_screen_res=1366x768&rp_floor=0.01&rp_secure=1&slots=1&rand=0.9529378579321524"

xhr=["https://www.mobile.de/svc/r/makes/Car?_lang=de",carro]

for data in xhr:
	get_data(data)
#https://suchen.mobile.de/fahrzeuge/svc/r/makes/Car


